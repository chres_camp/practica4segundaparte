<!DOCTYPE html>
<html lang="es">
<head>
<title>Comparaciones</title>
<link rel="stylesheet" href="css/tabs.css"/>
<link rel="stysheet" href="css/delimeters.css"/>
<meta charset="UTF-8">

</head>
<body>
<header>
<center>
<h1>Ejemplo de comparaciones en PHP </h1>
<center>
</header>
<section>
	<article>
	
	
		
		<?php
		
		/*
		== estamos diciendole que si a es igual a b
		!= es diferente de, es decir que si "a" es diferente de "b"
		<  comparacion, donde le decimos que a es menor que b 
		> al igual que el anterior, pero a es mayor que b
		>= en este caso decimos que a es mayor o igual a c
		<= y vice versa, a es menor o igual a c
		
		*/
			$a = 8;
			$b = 3;
			$c = 3;
			echo $a == $b, "<br>";
			echo $a != $b, "<br>";
			echo $a < $b, "<br>";
			echo $a > $b, "<br>";
			echo $a >= $c, "<br>";
			echo $a <= $c, "<br>";

		?>
		
	</article>
</section>


</body>
</html>
